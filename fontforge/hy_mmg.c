/****************************************************
*													*
*					hy_mmg.c						*
*													*
****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <utype.h>
#include <ustring.h>
#include <gfile.h>
#include "fontforge.h"
#include "fontforgeui.h"
#include "splinefont.h"

#include "hy_glyphutils.h"
#include "hy_mmg.h"

void HY_CheckMMGFont( SplineFont *sf )
{
	int bfirst = 1;
	int i;
	int layer = sf->layer_cnt - 1;	// except guide layer, back layer

	sf->bMMGFont = 0;
	sf->mgstartPos = 0;
	sf->mgendPos = 0;
	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		if( sf->map->map[i] != -1 && SCWorthOutputting(sf->glyphs[sf->map->map[i]]) )
		{
			if( sf->glyphs[sf->map->map[i]]->bmaster != 0 )
			{
				sf->bMMGFont = 1;
				if( bfirst == 1 )
				{
					sf->mgstartPos = i;
					bfirst = 0;
				}
				else
				{
					sf->mgendPos = i;
				}
			}
		}
	}

	if( sf->bMMGFont != 1 )
	{
		if( sf->layer_cnt > 3 )
		{
			while( sf->layer_cnt != 2 )
			{
				SFRemoveLayer( sf, layer );
				layer--;
			}
		}
	}
}

void HY_FitAllMMGBounds( SplineChar * sc )
{
	int layer;
	DBounds bb[2];
	real t[6] = { 1, 0, 0, 1, 0, 0 };

	if( sc->bmaster )
	{
		for( layer = ly_fore ; layer < sc->layer_cnt ; layer++ )
		{
			if( sc->layers[layer].splines != NULL )
			{
				SplineCharLayerFindBounds( sc, ly_fore, &bb[0] );
				SplineCharLayerFindBounds( sc, layer, &bb[1] );
				t[4] = bb[0].minx - bb[1].minx;
				t[5] = bb[0].maxy - bb[1].maxy;
				SplinePointListTransform( sc->layers[layer].splines, t, true );
			}
		}
	}
}


// END OF FILE

