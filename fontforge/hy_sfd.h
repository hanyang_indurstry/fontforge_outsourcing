/****************************************************
*													*
*					hy_sfd.h							*
*													*
****************************************************/

#ifndef HY_SFD_H
#define HY_SFD_H

#include "views.h"



extern int HY_MMFNormalSFDDump( FILE *sfd, SplineFont *sf, EncMap *map, EncMap *normal, int todir, char *dirname );
extern int HY_MMSFDWrite( char * filename, SplineFont *sf, EncMap *map, EncMap *normal, int todir );

#endif			/* HY_SFD_H */

