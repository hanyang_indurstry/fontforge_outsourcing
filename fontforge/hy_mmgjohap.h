/****************************************************
*													*
*					hy_mmgjohap.h					*
*													*
****************************************************/

#ifndef HY_MMGJOHAP_H
#define HY_MMGJOHAP_H

#include "views.h"
#include "splinefont.h"


#define EZJOHAP_DBG			0
#define MAIN_DBG			0
#define TRANS_DBG			0

#define JOHAP_TBL_DBG		0
#define JUNG_HORI_DBG		0
#define JUNG_VERT_DBG		0
#define JUNG_COMP_DBG		0

#define DO_MIN_GROUP
#define DO_HORI_GROUP		1
#define DO_VERT_GROUP		1
#define DO_COMP_GROUP		1

#define NUM_OF_CHO			19
#define NUM_OF_JUNG		21
#define NUM_OF_JONG		28

#define JUNG_TYPE_HORI		1
#define JUNG_TYPE_VERT		2
#define JUNG_TYPE_COMP		3

#define MG_TYPE_NONE		0
#define MG_TYPE_CHO		1
#define MG_TYPE_JUNG		2
#define MG_TYPE_JONG		3

static int jungsung_type[21] =
{
	1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 3, 3, 2, 2, 3, 3, 3, 2, 2, 3, 1
};

enum ju_type
{
	ju_horizon = 0,
	ju_vertical,
	ju_complex
};


////////////////////////////////////////////////////////////////////////////////////////////////////
//								FUNCTION DEFINITIONS
////////////////////////////////////////////////////////////////////////////////////////////////////

extern void HY_HTypeJohap( SplineFont *sf, int uni, int type, int cho, int jung, int jong);
extern void HY_VTypeJohap( SplineFont *sf, int uni, int type, int cho, int jung, int jong);
extern void HY_CTypeJohap( SplineFont *sf, int uni, int type, int cho, int jung, int jong);
extern void HY_GetBaseCharTransInfo( SplineFont *sf, int index, int mmgtype, DBounds *bounds, real trans[6] );
extern void HY_MakeNoneBaseChar( SplineFont *sf, int index, int jungtype, int mmgtype, DBounds *bounds, real trans[6] );
extern void HY_AddLinkToMG( SplineFont *sf, int from, int into, int mmgtype, int copytrans );
extern void HY_MMGJohapApplyTrans( SplineFont *sf, int index, int jungtype, int mmgtype, DBounds *bb1, real t1[6], DBounds *bb2, real t2[6], DBounds *bb3, real t3[6] );
extern void HY_MMGJohapApplyTransNoneJong( SplineFont *sf, int index, int jungtype, int mmgtype, DBounds *bb1, real t1[6], DBounds *bb2, real t2[6], DBounds *bb3, real t3[6] );
extern void HY_MMGJohapApplyTransBase( SplineFont *sf, int index, int jungtype, int mmgtype, DBounds *bb1, real t1[6], DBounds *bb2, real t2[6], DBounds *bb3, real t3[6] );


#endif			/* HY_MMGJOHAP_H */

