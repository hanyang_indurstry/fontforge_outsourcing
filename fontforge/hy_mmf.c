/****************************************************
*													*
*					hy_mmf.c						*
*													*
****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <utype.h>
#include <ustring.h>
#include <gfile.h>
#include "fontforge.h"
#include "fontforgeui.h"
#include "splinefont.h"

#include "hy_glyphutils.h"
#include "hy_mmf.h"


void HY_MMGBlendChar( MMSet * mm, int gid )
{
	int i;
	DBounds instbb[MmMax];
	DBounds fixedbb = { 0.0, 0.0, 0.0, 0.0 };

	if( HY_GetRefCharCount(mm->instances[0]->glyphs[gid]) == 0)
	{
		return;
	}

	if( HY_GetRefCharCount(mm->instances[1]->glyphs[gid]) == 0)
	{
		return;
	}

	if( !SCWorthOutputting(mm->instances[0]->glyphs[gid]) )
	{
		return;
	}

	if( !SCWorthOutputting(mm->instances[1]->glyphs[gid]) )
	{
		return;
	}

	if( mm->instances[0]->glyphs[gid]->unicodeenc != -1
		|| mm->instances[1]->glyphs[gid]->unicodeenc != -1
		|| mm->instances[0]->glyphs[gid]->layers[ly_fore].refs->sc->bmaster != 1
		|| mm->instances[1]->glyphs[gid]->layers[ly_fore].refs->sc->bmaster != 1 )
	{
		return;
	}

	for ( i = 0 ; i < mm->instance_count ; ++i )
	{
		SplineCharLayerFindBounds( mm->instances[i]->glyphs[gid], ly_fore, &instbb[i] );

		// org		
		fixedbb.minx += instbb[i].minx * mm->defweights[i];
		fixedbb.maxx += instbb[i].maxx * mm->defweights[i];
		fixedbb.miny += instbb[i].miny * mm->defweights[i];
		fixedbb.maxy += instbb[i].maxy * mm->defweights[i];

		/*
		// test
		fixedbb.minx += instbb[i].minx * mm->defweights[i] * 0.5;
		fixedbb.maxx += instbb[i].maxx * mm->defweights[i] * 0.5;
		fixedbb.miny += instbb[i].miny * mm->defweights[i];
		fixedbb.maxy += instbb[i].maxy * mm->defweights[i];
		*/
	}

	mm->normal->glyphs[gid]->fixedbb.minx = fixedbb.minx;
	mm->normal->glyphs[gid]->fixedbb.maxx = fixedbb.maxx;
	mm->normal->glyphs[gid]->fixedbb.miny = fixedbb.miny;
	mm->normal->glyphs[gid]->fixedbb.maxy = fixedbb.maxy;
}

void HY_MMGBlendChar2( MMSet * mm, int gid )
{
	DBounds masterbb;
	real xscale = 1.0, yscale = 1.0, dx = 0.0, dy = 0.0;

	if( HY_GetRefCharCount(mm->normal->glyphs[gid]) == 0 || !SCWorthOutputting(mm->normal->glyphs[gid]) )
	{
		return;
	}

	if( mm->normal->glyphs[gid]->unicodeenc == -1 && mm->instances[0]->glyphs[gid]->layers[ly_fore].refs->sc->bmaster == 1 && mm->instances[1]->glyphs[gid]->layers[ly_fore].refs->sc->bmaster == 1 )
	{
		// master glyph bounding box
		SplineCharLayerFindBounds( mm->normal->glyphs[gid]->layers[ly_fore].refs->sc, ly_fore, &masterbb );

		// scale, dx, dy
		xscale = (mm->normal->glyphs[gid]->fixedbb.maxx - mm->normal->glyphs[gid]->fixedbb.minx) / (masterbb.maxx - masterbb.minx);
		yscale = (mm->normal->glyphs[gid]->fixedbb.maxy - mm->normal->glyphs[gid]->fixedbb.miny) / (masterbb.maxy - masterbb.miny);
		dx = ((1 - xscale) * masterbb.minx) - masterbb.minx + mm->normal->glyphs[gid]->fixedbb.minx;
		dy = ((1 - yscale) * masterbb.miny) - masterbb.miny + mm->normal->glyphs[gid]->fixedbb.miny;


		mm->normal->glyphs[gid]->layers[ly_fore].refs->transform[0] = xscale;
		mm->normal->glyphs[gid]->layers[ly_fore].refs->transform[1] = 0;
		mm->normal->glyphs[gid]->layers[ly_fore].refs->transform[2] = 0;
		mm->normal->glyphs[gid]->layers[ly_fore].refs->transform[3] = yscale;
		mm->normal->glyphs[gid]->layers[ly_fore].refs->transform[4] = dx;
		mm->normal->glyphs[gid]->layers[ly_fore].refs->transform[5] = dy;
	}
}

// END OF FILE

