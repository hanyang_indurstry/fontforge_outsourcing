/****************************************************
*													*
*					hy_glyphutils.c					*
*													*
****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <utype.h>
#include <ustring.h>
#include <gfile.h>
#include "fontforge.h"
#include "fontforgeui.h"
#include "splinefont.h"

#include "hy_glyphutils.h"
#include "hy_ksc5601.h"
#include "hy_sjis.h"

#include <unistd.h>

// sc1: hangul unicode glyph
// sc2: master glyph
int HY_IsDependents( SplineChar * sc1, SplineChar *sc2 )
{
	struct splinecharlist * dep;

	for( dep = sc2->dependents ; dep != NULL ; dep = dep->next )
	{
		if( (strcmp( dep->sc->name, sc1->name ) == 0) && (dep->sc->unicodeenc == sc1->unicodeenc) )
		{
			return( true );
		}
	}
	return( false );
}

uint16 HY_GetKSC5601( uint16 unicode )
{
	int index1 = unicode / 256;
	int index2 = unicode % 256;

	if( index1 < 256 && index2 < 256 )
	{
		return( ksc5601_from_unicode_[index1][index2] );
	}
	else
	{
		return( 0x0000 );
	}
}

int HY_GetSCGIDBySC( SplineFont * sf, SplineChar * sc )
{
	int i, gid = -1;

	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		gid = sf->map->map[i];
		if( gid != -1 && SCWorthOutputting(sf->glyphs[gid]) )
		{
			if( strcmp(sf->glyphs[gid]->name, sc->name) == 0 )
			{
				return( gid );
			}
		}
	}
	return( gid );
}

int HY_GetSCEncBySC( SplineFont * sf, SplineChar * sc )
{
	int i, enc = -1;

	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		if( sf->map->map[i] != -1 && (SCWorthOutputting(sf->glyphs[sf->map->map[i]])) )
		{
			if( strcmp(sf->glyphs[sf->map->map[i]]->name, sc->name) == 0 )
			{
				enc = i;
				break;
			}
		}
	}
	return( enc );
}

int HY_GetSCEncByName( SplineFont * sf, char * name )
{
	int i, enc = -1;

	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		if( sf->map->map[i] != -1 && (SCWorthOutputting(sf->glyphs[sf->map->map[i]])) )
		{
			if( strcmp(name, sf->glyphs[sf->map->map[i]]->name) == 0 )
			{
				enc = i;
				break;
			}
		}
	}
	return( enc );
}

void HY_AddLink( SplineChar * sc, SplineChar * rsc, real transform[6] )
{
	RefChar * ref;

	if( sc == NULL || rsc == NULL )
	{
		return;
	}

	if( sc == rsc )
	{
		return;
	}

	ref						= RefCharCreate();
	ref->sc					= rsc;
	ref->unicode_enc			= rsc->unicodeenc;
	ref->orig_pos				= rsc->orig_pos;
	ref->adobe_enc			= getAdobeEnc( rsc->name );
	ref->next				= sc->layers[ly_fore].refs;
	ref->transform[0]			= transform[0];
	ref->transform[1]			= transform[1];
	ref->transform[2]			= transform[2];
	ref->transform[3]			= transform[3];
	ref->transform[4]			= transform[4];
	ref->transform[5]			= transform[5];
	sc->layers[ly_fore].refs	= ref;

	SCReinstanciateRefChar( sc, ref, ly_fore );
	SCMakeDependent( sc, rsc );
}

void HY_MGAddLink( SplineChar * sc, SplineChar * rsc )
{
	RefChar *ref;
	real t[6];

	if ( sc == NULL || rsc == NULL )
	{
		return;
	}

	if( sc == rsc )
	{
		return;
	}

	t[0] = 1.0;
	t[1] = 0.0;
	t[2] = 0.0;
	t[3] = 1.0;
	t[4] = 0.0;
	t[5] = 0.0;

	ref						= RefCharCreate();
	ref->sc					= rsc;
	ref->unicode_enc			= rsc->unicodeenc;
	ref->orig_pos				= rsc->orig_pos;
	ref->adobe_enc			= getAdobeEnc( rsc->name );
	ref->next				= sc->layers[ly_fore].refs;
	ref->transform[0]			= t[0];
	ref->transform[1]			= t[1];
	ref->transform[2]			= t[2];
	ref->transform[3]			= t[3];
	ref->transform[4]			= t[4];
	ref->transform[5]			= t[5];
	sc->layers[ly_fore].refs	= ref;

	SCReinstanciateRefChar( sc, ref, ly_fore );
	SCMakeDependent( sc, rsc );
}

void HY_SCAddKoreanRef( SplineChar * sc, SplineChar * rsc )
{
	RefChar *ref;
	real t[6];

	if ( rsc == NULL )
	{
		return;
	}

	if( sc == rsc )
	{
		return;
	}

	t[0] = 1.0;
	t[1] = 0.0;
	t[2] = 0.0;
	t[3] = 1.0;
	t[4] = 0.0;
	t[5] = 0.0;

	ref						= RefCharCreate();
	ref->sc					= rsc;
	ref->unicode_enc			= rsc->unicodeenc;
	ref->orig_pos				= rsc->orig_pos;
	ref->adobe_enc			= getAdobeEnc( rsc->name );
	ref->next				= sc->layers[ly_fore].refs;
	sc->layers[ly_fore].refs	= ref;
#if 0
	memcpy( ref->transform, t, sizeof(real [6]) );
#else
	ref->transform[0]			= t[0];
	ref->transform[1]			= t[1];
	ref->transform[2]			= t[2];
	ref->transform[3]			= t[3];
	ref->transform[4]			= t[4];
	ref->transform[5]			= t[5];
#endif

	//HY_RuleJohapSCReinstanciateRefChar( sc, ref, ly_fore );
	SCReinstanciateRefChar( sc, ref, ly_fore );
	SCMakeDependent( sc, rsc );
}

void HY_SCAddKoreanRef_New( SplineChar * sc, SplineChar * rsc, real t[6] )
{
	RefChar *ref;

	if ( rsc == NULL )
	{
		return;
	}

	if( sc == rsc )
	{
		return;
	}

	ref						= RefCharCreate();
	ref->sc					= rsc;
	ref->unicode_enc			= rsc->unicodeenc;
	ref->orig_pos				= rsc->orig_pos;
	ref->adobe_enc			= getAdobeEnc( rsc->name );
	ref->next				= sc->layers[ly_fore].refs;
	sc->layers[ly_fore].refs	= ref;

	ref->transform[0]			= t[0];
	ref->transform[1]			= t[1];
	ref->transform[2]			= t[2];
	ref->transform[3]			= t[3];
	ref->transform[4]			= t[4];
	ref->transform[5]			= t[5];

	SCReinstanciateRefChar( sc, ref, ly_fore );
	SCMakeDependent( sc, rsc );
}

int HY_GetRefCharCount( SplineChar *sc )
{
	int			num = 0;
	RefChar *	rf;

	if( sc == NULL )
	{
		return num;
	}

	if( sc->layers[ly_fore].refs == NULL )
	{
		return num;
	}

	for( rf = sc->layers[ly_fore].refs ; rf != NULL ; rf = rf->next )
	{
		num++;
	}
	return num;
}

void HY_SFDFixupRef( SplineChar * sc, RefChar * ref, int layer )
{
    RefChar *rf;

	for ( rf = ref->sc->layers[layer].refs ; rf != NULL ; rf = rf->next )
	{
		if ( rf->sc == sc )
		{
			ref->sc->layers[layer].refs = NULL;
			break;
		}

		if ( rf->layers[0].splines == NULL )
		{
			HY_SFDFixupRef(ref->sc,rf,layer);
		}
	}
	SCReinstanciateRefChar(sc,ref,layer);
	SCMakeDependent(sc,ref->sc);
}

void HY_SFDFixupRefs( SplineFont * sf )
{
	int i;
	int layer = ly_fore;
	RefChar * refs;

	for ( i=0; i<sf->glyphcnt; ++i )
	{
		if ( sf->glyphs[i]!=NULL )
		{
			SplineChar *sc = sf->glyphs[i];
			for ( layer=0; layer<sc->layer_cnt; ++layer )
			{
				for ( refs = sf->glyphs[i]->layers[layer].refs; refs!=NULL; refs=refs->next )
				{
					HY_SFDFixupRef(sf->glyphs[i],refs,layer);
				}
			}
		}
	}
}

int HY_GetDependentCount( SplineChar *sc )
{
	int count = 0;
	struct splinecharlist * dep;

	if( sc == NULL )
	{
		return count;
	}

	for( dep = sc->dependents ; dep != NULL ; dep = dep->next )
	{
		count++;
	}
	return count;
}

void HY_AddJasoGlyphDependents( SplineFont * from, SplineFont * into, SplineChar * radsc, SplineChar * sc )
{
	int i, j, jaso_index, dep_index;
	struct splinecharlist * dlist;
	real transform[6];

	jaso_index = HY_GetSCEncBySC( from, radsc );

	for( i = 0 ; i < from->map->enccount ; i++ )
	{
		if( i == jaso_index )
		{
			if( from->map->map[i] != -1 && SCWorthOutputting(from->glyphs[from->map->map[i]]) )
			{
				for( dlist = from->glyphs[from->map->map[i]]->dependents ; dlist != NULL ; dlist = dlist->next )
				{
					dep_index = HY_GetSCEncBySC( from, dlist->sc );

					for( j = 0 ; j < into->map->enccount ; j++ )
					{
						if( j == dep_index )
						{
							//if( into->map->map[j] != -1 && into->glyphs[into->map->map[j]] != NULL )
							if( into->map->map[j] != -1 && SCWorthOutputting(into->glyphs[into->map->map[j]]) )
							{
								transform[0] = dlist->sc->layers[ly_fore].refs->transform[0];
								transform[1] = dlist->sc->layers[ly_fore].refs->transform[1];
								transform[2] = dlist->sc->layers[ly_fore].refs->transform[2];
								transform[3] = dlist->sc->layers[ly_fore].refs->transform[3];
								transform[4] = dlist->sc->layers[ly_fore].refs->transform[4];
								transform[5] = dlist->sc->layers[ly_fore].refs->transform[5];

								HY_AddLink( into->glyphs[into->map->map[j]], sc, transform );
							}
							else
							{
								SFMakeChar( into, into->map, j );
								
								transform[0] = dlist->sc->layers[ly_fore].refs->transform[0];
								transform[1] = dlist->sc->layers[ly_fore].refs->transform[1];
								transform[2] = dlist->sc->layers[ly_fore].refs->transform[2];
								transform[3] = dlist->sc->layers[ly_fore].refs->transform[3];
								transform[4] = dlist->sc->layers[ly_fore].refs->transform[4];
								transform[5] = dlist->sc->layers[ly_fore].refs->transform[5];

								HY_AddLink( into->glyphs[into->map->map[j]], sc, transform );
							}
						}
					}
				}
			}
		}
	}
}

// END OF FILE
