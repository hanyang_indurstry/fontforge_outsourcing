/****************************************************
*													*
*				hy_mmgtransformh					*
*													*
****************************************************/

#ifndef HY_MMGTRANSFORM_H
#define HY_MMGTRANSFORM_H


#include "views.h"


extern int HY_getNumSplines_SplinePointList( SplinePointList *sp );
extern SplinePointList * HY_MMGSplinePointListTranslate(SplinePointList *base,real dx, real dy,int allpoints);
extern SplinePointList * HY_MMGSplinePointListTransform(RefChar *ref, SplinePointList *spl, real transform[6], int allpoints);

#endif			/* HY_MMGTRANSFORM_H */

