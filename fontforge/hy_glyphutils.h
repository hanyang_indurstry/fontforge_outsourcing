/****************************************************
*													*
*					hy_glyphutils.h					*
*													*
****************************************************/

#ifndef HY_GLYPHUTILS_H
#define HY_GLYPHUTILS_H

#include "views.h"


extern uint16 HY_GetKSC5601( uint16 unicode );
extern int HY_IsDependents( SplineChar * sc1, SplineChar *sc2 );
extern int HY_GetSCGIDBySC( SplineFont * sf, SplineChar * sc );
extern int HY_GetSCEncBySC( SplineFont * sf, SplineChar * sc );
extern int HY_GetSCEncByName( SplineFont * sf, char * name );

extern void HY_AddLink( SplineChar * sc, SplineChar * rsc, real transform[6] );
extern void HY_MGAddLink( SplineChar * sc, SplineChar * rsc );
extern void HY_SCAddKoreanRef( SplineChar * sc, SplineChar * rsc );
extern void HY_SCAddKoreanRef_New( SplineChar * sc, SplineChar * rsc, real t[6] );
extern void HY_AddJasoGlyphDependents( SplineFont * from, SplineFont * into, SplineChar * radsc, SplineChar * sc );
extern void HY_SFDFixupRef( SplineChar * sc, RefChar * ref, int layer );
extern void HY_SFDFixupRefs( SplineFont * sf );
extern int HY_GetRefCharCount( SplineChar *sc );
extern int HY_GetDependentCount( SplineChar * sc );

#endif			/* HY_GLYPHUTILS_H */
