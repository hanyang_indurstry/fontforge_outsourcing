/****************************************************
*													*
*				hy_mmgtransform.c					*
*													*
****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <utype.h>
#include <ustring.h>
#include <gfile.h>
#include "fontforge.h"
#include "fontforgeui.h"
#include "splinefont.h"

#include "hy_mmgtransform.h"


int HY_getNumSplines_SplinePointList( SplinePointList *sp )
{
	SplinePointList *spl;
	int numSplines = 0;

	if (sp == NULL)
	{
		return 0;
	}

	for ( spl = sp; spl!=NULL; spl = spl->next )
	{
		numSplines++;
	}
	return numSplines;
}

SplinePointList * HY_MMGSplinePointListTranslate(SplinePointList *base,real dx, real dy,int allpoints)
{
	real transform[6];

	transform[0] = transform[3] = 1;
	transform[1] = transform[2] = transform[5] = 0;
	transform[4] = dx;
	transform[5] = dy;

	return( SplinePointListTransform(base, transform, allpoints) );
}

SplinePointList * HY_MMGSplinePointListTransform(RefChar *ref, SplinePointList *spl, real transform[6], int allpoints)
{
	SplineChar dummy;
	SplinePointList * splines[9] = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };
	DBounds bb[9];
	DBounds bbox;
	real xamount = 0.0;
	real yamount = 0.0;
	int nsp2 = 0;
	int nsp3 = 0;
	int nsp4 = 0;
	int nsp5 = 0;
	int layer_cnt = ref->sc->layer_cnt;
	int layer;

	// Check if refChar has baseGlyph , Do sequtiry routine  
	if( layer_cnt < 3 )
	{
		return SplinePointListTransform(spl, transform, allpoints);
	}

	// check small , wide, tall layer splines
	if( ref->sc->layers[2].splines == NULL || ref->sc->layers[3].splines == NULL || ref->sc->layers[4].splines == NULL )
	{
		return SplinePointListTransform(spl, transform, allpoints);
	}


	// get number of splines in SplinePointLists
	nsp2 = HY_getNumSplines_SplinePointList(ref->sc->layers[2].splines);
	nsp3 = HY_getNumSplines_SplinePointList(ref->sc->layers[3].splines);
	nsp4 = HY_getNumSplines_SplinePointList(ref->sc->layers[4].splines);
	if( ref->sc->layer_cnt > 5 )
	{
		nsp5 = HY_getNumSplines_SplinePointList(ref->sc->layers[5].splines);
	}

	// check if base glyphs have equal number of splines
	if( (nsp3 && (nsp2 != nsp3)) || (nsp4 && (nsp2 != nsp4)) || (nsp5 && (nsp2 != nsp5)) )
	{
		return SplinePointListTransform(spl, transform, allpoints);
	}

	// base glyph의 4개의 글립을 임시 splines에 카피해 옵니다.
	if(ref->sc->layers[2].splines)
	{
		splines[2] = SplinePointListCopy(ref->sc->layers[2].splines);   // small
	}

	if(ref->sc->layers[3].splines)
	{
		splines[3] = SplinePointListCopy(ref->sc->layers[3].splines);   // wide
	}

	if(ref->sc->layers[4].splines)
	{
		splines[4] = SplinePointListCopy(ref->sc->layers[4].splines);   // tall
	}

	if( layer_cnt > 5 )
	{
		if(ref->sc->layers[5].splines)
		{
			splines[5] = SplinePointListCopy(ref->sc->layers[5].splines);   // big
		}
	}

	SplineSetFindBounds(splines[2], &bb[2]);
	SplineSetFindBounds(splines[3], &bb[3]);
	SplineSetFindBounds(splines[4], &bb[4]);
	if( splines[5] != NULL )
	{
		SplineSetFindBounds(splines[5], &bb[5]);
	}

	// 변형하고자 하는 크기로 입력된 spline을 변형해서, BBOX를 측정합니다.
	SplinePointListTransform(spl, transform, allpoints);
	SplineSetFindBounds(spl, &bbox);

	// 변형된 BOX의 좌측상단으로 base glyph들을 정렬합니다.
	if( splines[2] != NULL )
	{
		splines[2] = HY_MMGSplinePointListTranslate(splines[2], bbox.minx-bb[2].minx, bbox.maxy-bb[2].maxy, true);
	}

	if( splines[3] != NULL )
	{
		splines[3] = HY_MMGSplinePointListTranslate(splines[3], bbox.minx-bb[3].minx, bbox.maxy-bb[3].maxy, true);
	}

	if( splines[4] != NULL )
	{
		splines[4] = HY_MMGSplinePointListTranslate(splines[4], bbox.minx-bb[4].minx, bbox.maxy-bb[4].maxy, true);
	}

	if( splines[5] != NULL )
	{
		splines[5] = HY_MMGSplinePointListTranslate(splines[5], bbox.minx-bb[5].minx, bbox.maxy-bb[5].maxy, true);
	}

	// 정렬을 마친 base glyph들의 BBOX를 다시 측정합니다.
	if( splines[2] != NULL )
	{
		SplineSetFindBounds(splines[2], &bb[2]);
	}
	if( splines[3] != NULL )
	{
		SplineSetFindBounds(splines[3], &bb[3]);
	}
	if( splines[4] != NULL )
	{
		SplineSetFindBounds(splines[4], &bb[4]);
	}

	if( splines[5] != NULL )
	{
		SplineSetFindBounds(splines[5], &bb[5]);
	}

	if( splines[2] != NULL )
	{
		// Interpolation 할 x,yamount 값을 계산합니다.
		if( splines[3] != NULL )
		{
			xamount = (bbox.maxx - bb[2].maxx) / (bb[3].maxx - bb[2].maxx);
		}

		if( splines[3] != NULL )
		{
			yamount = (bbox.miny - bb[2].miny) / (bb[4].miny - bb[2].miny);
		}

#if 0
		// Adjust x,yamount :: KSK ADD :: 보다 정확한 interpolation 결과를 위해서.
		if(xamount < 0.0) xamount = 0.0;     
		if(yamount < 0.0) yamount = 0.0;     
		if(xamount > 1.0) xamount = 1.0;     
		if(yamount > 1.0) yamount = 1.0;     
#endif

		// Interpolation 합니다.
		if( splines[5] != NULL )
		{
			// 4 base_glyph Interpolation
			splines[6] = SplineSetsInterpolate(splines[2], splines[3], xamount, &dummy);
			splines[7] = SplineSetsInterpolate(splines[4], splines[5], xamount, &dummy);
			splines[8] = SplineSetsInterpolate(splines[6], splines[7], yamount, &dummy);
		}
		else
		{
			// 3 base_glyph Interpolation
			splines[6] = SplineSetsInterpolate(splines[2], splines[3], xamount*2.0, &dummy);
			splines[7] = SplineSetsInterpolate(splines[2], splines[4], yamount*2.0, &dummy);
			splines[8] = SplineSetsInterpolate(splines[6], splines[7], 0.5, &dummy);
		}   // KSK 0921
	}

	// Interpolation된 결과를 입력받은 spline에 카피합니다.
	if(splines[8])
	{
		SplinePointListSet(spl, splines[8]);
	}

	// 사용한 임시 spline들을 메모리에서 제거합니다.
	if( splines[2] != NULL ) SplinePointListsFree(splines[2]); 
	if( splines[3] != NULL ) SplinePointListsFree(splines[3]); 
	if( splines[4] != NULL ) SplinePointListsFree(splines[4]); 
	if( splines[5] != NULL ) SplinePointListsFree(splines[5]); 
	if( splines[6] != NULL ) SplinePointListsFree(splines[6]); 
	if( splines[7] != NULL ) SplinePointListsFree(splines[7]);
	if( splines[8] != NULL ) SplinePointListsFree(splines[8]);

	return( spl ); //KSK3 ADD
}


// END OF FILE

