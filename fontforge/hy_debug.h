/********************************************************
 *														*
 *						hy_debug.h						*
 *														*
 ********************************************************/
#ifndef HY_DEBUG_H
#define HY_DEBUG_H


#define CLOSE	0
#define hydebug(fmt,args...)		printf( "" fmt, ## args )


#endif	// HY_DEBUG_H

