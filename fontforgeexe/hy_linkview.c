/****************************************************
*													*
*					hy_linkview.c						*
*													*
****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <utype.h>
#include <ustring.h>
#include <gfile.h>
#include <gkeysym.h>
#include "fontforge.h"
#include "fontforgeui.h"
#include "splinefont.h"

#include "hy_glyphutils.h"
#include "hy_linkview.h"
#include "hy_debug.h"


#define LINKVIEW_DEBUG	0

#define NUM_JASO_FC	19
#define NUM_JASO_MV	21
#define NUM_JASO_LC	28

static Color selcol = 0xffff00;
static Color bgcol = 0xffffff;
static Color errbgcol = 0xffffff;
static Color fvselcol = 0xffff00;
static Color fvselfgcol = 0x000000;
static Color bbcol = 0x808080;
static Color ksc5601col = 0xafeeee;
static Color bindocol = 0xf0bcbc;
static int viewWidth = DEFAULT_COMPOSITE_GLYFS_VIEW_WIDTH;
static int hy_use_freetype_to_rasterize_rgv = 1;
static int s_glyphsize = 0;

static int ChoSungIdx[19] = 
{
	0, 1, 3, 6, 7, 8,16,17,18,20,
	21,22,23,24,25,26,27,28,29
};

static int JungSungIdx[21] = 
{
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
	10, 11,12,13,14,15,16,17,18,19,
	20
};

static int JongSungIdx[27] =
{
	0, 1, 2, 3, 4, 5,    6, 8, 9,
	10,11,12,13,14,15,16,17,   19,
	20,21,22,23,25,26,27,28,29
};


static void HY_LinkViewMovePageIndex( CharView * cv, int next );
static void HY_LinkViewSetPageIndex( CharView * cv, int index );
static void HY_LinkViewCharResize( CharView * cv );
static void HY_LinkViewResize( CharView * cv );
static void HY_LinkViewSelectGlyph( CharView * cv, int offset );
static void HY_LinkViewExpose( GWindow pixmap, CharView *cv, GEvent *event );
static void HY_LinkViewVScroll(CharView *cv, struct sbevent *sb);



static void HY_LinkViewMovePageIndex( CharView * cv, int next )
{
	if( next )
	{
		cv->linkview->scroll_off++;
		if( cv->linkview->scroll_off > cv->linkview->pagecount - 1 )
		{
			cv->linkview->scroll_off = cv->linkview->pagecount - 1;
		}
		HY_LinkViewSetPageIndex( cv, cv->linkview->scroll_off );
		GScrollBarSetPos(cv->linkview->vsb, cv->linkview->scroll_off);
		GDrawScroll(cv->linkview->gw, NULL, 0, 0);
	}
	else
	{
		cv->linkview->scroll_off--;
		if( cv->linkview->scroll_off < 0 )
		{
			cv->linkview->scroll_off = 0;
		}
		HY_LinkViewSetPageIndex(cv, cv->linkview->scroll_off);
		GScrollBarSetPos(cv->linkview->vsb, cv->linkview->scroll_off);
		GDrawScroll(cv->linkview->gw, NULL, 0, 0);
	}
}

static void HY_LinkViewVScroll(CharView *cv, struct sbevent *sb)
{
	int newpos = cv->linkview->scroll_off;

	HY_LinkViewGetPageCount(cv);

	switch( sb->type )
	{
		case et_sb_top:
		{
			newpos = 0;
		}
		break;
		case et_sb_uppage:
		{
			newpos--;
		}
		break;
		case et_sb_up:
		{
			newpos--;
		}
		break;
		case et_sb_down:
		{
			newpos++;
		}
		break;
		case et_sb_downpage:
		{
			newpos++;
		}
		break;
		case et_sb_bottom:
		{
			newpos = cv->linkview->rowcnt;
		}
		break;
		case et_sb_thumb:
		case et_sb_thumbrelease:
		{
			newpos = sb->pos;
		}
		break;
	}

	if( newpos < 0 )
	{
		newpos = 0;
	}

	if( newpos > cv->linkview->pagecount - 1 )
	{
		newpos = cv->linkview->pagecount - 1;
	}

	if( newpos != cv->linkview->scroll_off )
	{
		cv->linkview->scroll_off = newpos;
		HY_LinkViewSetPageIndex( cv, cv->linkview->scroll_off );
		GScrollBarSetPos(cv->linkview->vsb, cv->linkview->scroll_off);
		GDrawScroll(cv->linkview->gw, NULL, 0, 0);
	}
}

static void HY_LinkViewSelectGlyph( CharView * cv, int offset )
{
	int gonext;
	if( offset > 0 )
	{
		gonext = 1;
	}
	else
	{
		gonext = 0;
	}

	cv->linkview->charindex += offset;
	if( cv->linkview->charindex > cv->linkview->depcount -1 )
	{
		cv->linkview->charindex = cv->linkview->depcount -1;
		cv->linkview->selindex = cv->linkview->lastpagecharcount - 1;
		HY_LinkViewMovePageIndex(cv, gonext);
	}
	else if( cv->linkview->charindex < 0 )
	{
		cv->linkview->charindex = 0;
		cv->linkview->selindex = 0;
	}
	else
	{
		cv->linkview->selindex += offset;
		if( cv->linkview->selindex > cv->linkview->maxcharcount -1 )
		{
			cv->linkview->selindex %= cv->linkview->maxcol;
			HY_LinkViewMovePageIndex(cv, gonext);
		}
		else if( cv->linkview->selindex < 0 )
		{
			cv->linkview->selindex = cv->linkview->maxcharcount - 1;
			HY_LinkViewMovePageIndex(cv, gonext);
		}
	}
}

void HY_LinkViewGetPageCount( CharView * cv )
{
	cv->linkview->depcount = HY_GetDependentCount(cv->b.sc );

	if( cv->linkview->depcount )
	{
		GRect size;

		cv->linkview->charcount			= 0;
		cv->linkview->maxcharcount		= 0;
		cv->linkview->lastlinecharcount		= 0;
		cv->linkview->lastpagecharcount	= 0;
		cv->linkview->pagecount			= 0;
		cv->linkview->rowcnt				= 1;

		GDrawGetSize(cv->linkview->gw, &size);
		cv->linkview->width = size.width;
		cv->linkview->height = size.height;
		cv->linkview->maxcol = cv->linkview->width / cv->linkview->size;

		if(cv->linkview->maxcol <= 0)
		{
			cv->linkview->maxcol = 1;
		}

		cv->linkview->maxrow = cv->linkview->height / (cv->linkview->size + cv->linkview->name_h);

		if(cv->linkview->maxrow <= 0)
		{
			cv->linkview->maxrow = 1;
		}

		cv->linkview->maxcharcount = cv->linkview->maxcol * cv->linkview->maxrow;
		cv->linkview->lastpagecharcount = cv->linkview->depcount % cv->linkview->maxcharcount;

		if( cv->linkview->depcount > cv->linkview->maxcol )
		{
			cv->linkview->rowcnt = cv->linkview->depcount / cv->linkview->maxcol;
			cv->linkview->lastlinecharcount = cv->linkview->depcount % cv->linkview->maxcol;
			if( cv->linkview->lastlinecharcount != 0 )
			{
				cv->linkview->rowcnt++;
			}
		}

		if( cv->linkview->rowcnt <= cv->linkview->maxrow )
		{
			cv->linkview->pagecount = 1;
		}
		else if( cv->linkview->rowcnt > cv->linkview->maxrow )
		{
			cv->linkview->pagecount = cv->linkview->depcount / cv->linkview->maxcharcount;
			if( cv->linkview->lastpagecharcount )
			{
				cv->linkview->pagecount += 1;
			}
		}

		cv->linkview->charcount = (cv->linkview->maxcharcount * (cv->linkview->pagecount - 1)) + cv->linkview->lastpagecharcount;

		if( cv->linkview->pageindex > cv->linkview->pagecount -1 )
		{
			cv->linkview->pageindex = cv->linkview->pagecount -1;
		}

		if( cv->linkview->pageindex < 0 )
		{
			cv->linkview->pageindex = 0;
		}

		if( cv->linkview->charindex > cv->linkview->depcount -1 )
		{
			cv->linkview->charindex = cv->linkview->depcount -1;
			cv->linkview->selindex = cv->linkview->lastpagecharcount - 1;
			cv->linkview->scroll_off = cv->linkview->pagecount - 1;
		}
	}
}

static void HY_LinkViewSetPageIndex( CharView * cv, int index )
{
	cv->linkview->pageindex = index;
}

static void HY_LinkViewCharResize( CharView * cv )
{
	int sbsize;

	sbsize = GDrawPointsToPixels(cv->linkview->gw, SCROLL_BAR_WIDTH);
	cv->linkview->bb_w = cv->linkview->size;
	cv->linkview->bb_h = cv->linkview->size + cv->linkview->name_h;
	cv->linkview->width = (cv->linkview->bb_w * cv->linkview->maxcol) + sbsize;
	cv->linkview->height = (cv->linkview->bb_h * cv->linkview->maxrow);

	GDrawResize( cv->linkview->gw, cv->linkview->width, cv->linkview->height );

	if( cv->linkview->pageindex > cv->linkview->pagecount -1 )
	{
		cv->linkview->pageindex = cv->linkview->pagecount -1;
	}

	if( cv->linkview->charindex > cv->linkview->depcount -1 )
	{
		cv->linkview->charindex = cv->linkview->depcount -1;
		cv->linkview->selindex = cv->linkview->lastpagecharcount - 1;
		cv->linkview->scroll_off = cv->linkview->pagecount - 1;
	}

	if( cv->linkview->charindex  > cv->linkview->maxcharcount )
	{
		cv->linkview->pageindex = cv->linkview->charindex  / cv->linkview->maxcharcount;
		cv->linkview->selindex = cv->linkview->charindex % cv->linkview->maxcharcount;
	}

	GGadgetMove(cv->linkview->vsb, cv->linkview->width - sbsize, 0);
	GGadgetResize(cv->linkview->vsb, sbsize, cv->linkview->height);
	GScrollBarSetBounds(cv->linkview->vsb, 0, cv->linkview->pagecount, 1 );
	GScrollBarSetPos(cv->linkview->vsb, cv->linkview->scroll_off);
	HY_LinkViewSetPageIndex(cv, cv->linkview->scroll_off);
}

static void HY_LinkViewResize( CharView * cv )
{
	int sbsize;

	HY_LinkViewGetPageCount(cv);
	sbsize = GDrawPointsToPixels(cv->linkview->gw, SCROLL_BAR_WIDTH);

	cv->linkview->width = (cv->linkview->bb_w * cv->linkview->maxcol) + sbsize;
	cv->linkview->height = (cv->linkview->bb_h * cv->linkview->maxrow);

	GDrawResize( cv->linkview->gw, cv->linkview->width, cv->linkview->height );

	if( cv->linkview->pageindex > cv->linkview->pagecount -1 )
	{
		cv->linkview->pageindex = cv->linkview->pagecount -1;
	}

	if( cv->linkview->charindex > cv->linkview->depcount -1 )
	{
		cv->linkview->charindex = cv->linkview->depcount -1;
		cv->linkview->selindex = cv->linkview->lastpagecharcount - 1;
		cv->linkview->scroll_off = cv->linkview->pagecount - 1;
	}

	if( cv->linkview->charindex  > cv->linkview->maxcharcount )
	{
		cv->linkview->pageindex = cv->linkview->charindex  / cv->linkview->maxcharcount;
		cv->linkview->selindex = cv->linkview->charindex % cv->linkview->maxcharcount;
	}

	GGadgetMove(cv->linkview->vsb, cv->linkview->width - sbsize, 0);
	GGadgetResize(cv->linkview->vsb, sbsize, cv->linkview->height );
	GScrollBarSetBounds(cv->linkview->vsb, 0, cv->linkview->pagecount, 1 );
	GScrollBarSetPos(cv->linkview->vsb, cv->linkview->scroll_off);
	HY_LinkViewSetPageIndex(cv, cv->linkview->scroll_off);
}

static void HY_DrawLinkViewGlyfs( GWindow pixmap, CharView *cv, SplineChar *sc, int pos, int gid, int x, int y )
{
	FontView *fv = (FontView *)cv->b.fv;
	struct _GImage	base;
	GImage			gi;

	GRect			old;
	GRect			bound;
	GRect			namebb;
	int				imgX;
	int				imgY;

	BDFFont *		bdf;
	BDFChar *		bdfc;

	int				active_layer			= ly_fore;
	int				flags_antialias		= true;
	int				flags_bbsized		= false;
	int				flags_strokedfont	= false;
	int				flags_multilayer		= false;

	bdf = malloc( sizeof(BDFFont) );

	bdf = SplineFontPieceMeal(	fv->b.sf,
								active_layer,
								rint(( cv->linkview->size * 72.0 ) / cv->linkview->dpi),
								cv->linkview->dpi,
								(flags_antialias ? pf_antialias : 0) | (flags_bbsized ? pf_bbsized : 0) | (hy_use_freetype_to_rasterize_rgv && !sc->parent->strokedfont && !sc->parent->multilayer ? pf_ft_nohints : 0),
								NULL );

	BDFPieceMeal( bdf, gid );

	if( bdf != NULL && gid < bdf->glyphcnt && gid != -1 && bdf->glyphs[gid] != NULL )
	{
		bdfc = bdf->piecemeal ? bdf->glyphs[gid] : BDFGetMergedChar( bdf->glyphs[gid] );

		memset( &gi, '\0', sizeof( gi ) );
		memset( &base, '\0' , sizeof( base ) );

		if ( bdfc->byte_data )
		{
			gi.u.image = &base;
			base.image_type = it_index;
		    	base.clut		= fv->show->clut;
		}

		base.trans			= 0;
		base.clut->trans_index	= 0;
		base.data			= bdfc->bitmap;
		base.bytes_per_line	= bdfc->bytes_per_line;
		base.width			= bdfc->xmax-bdfc->xmin+1;
		base.height			= bdfc->ymax-bdfc->ymin+1;

		namebb.x			= bound.x;
		namebb.y			= bound.y + bound.height;
		namebb.width			= bdf->pixelsize;
		namebb.height		= cv->linkview->name_h;

		// bounding box rect
		bound.x				= x;
		bound.y				= y;
		bound.width			= bdf->pixelsize;
		bound.height			= bdf->pixelsize + namebb.height;

		/*********************************************************************************************/
		// Draw Hangul Jahap Character
		int uni = sc->unicodeenc;
		unichar_t buf[60];
		char utf8_buf[128];
		FontRequest rq;
		GFont * font;
		int as, ds, ld;
		int textX, textY;
		int txtWidth = 0;

		buf[0] = buf[1] = 0;

		if( cv->linkview->viewmode == VIEW_HANGUL )
		{
			char * pt = utf8_buf;
			if( sc->unicodeenc != -1 )
			{
				pt = utf8_idpb( pt, uni, 0 );
				*pt = '\0';
			}
			else
			{
				struct splinecharlist *dlist;
				SplineChar * depsc;
				char * jasobuf = utf8_buf;
				if( sc != NULL )
				{
					if( sc->unicodeenc == -1 )
					{
						if( HY_GetDependentCount(sc) )
						{
							for( dlist = sc->dependents ; dlist != NULL ; dlist = dlist->next )
							{
								depsc = dlist->sc;
							}
							jasobuf = utf8_idpb( jasobuf, depsc->unicodeenc, 0 );
						}
						else
						{
							jasobuf = utf8_idpb( jasobuf, -1, 0 );
						}
						*jasobuf = '\0';
					}
				}

				#if 0
				buf[0] = 0x0020;
				if( sc->bCompositionUnit )
				{
					int vcode = sc->VCode;
					if( vcode < NUM_JASO_FC )
					{
						buf[1] = 0x3131 + ChoSungIdx[vcode];
					}
					else if( vcode < NUM_JASO_FC + NUM_JASO_MV )
					{
						buf[1] = 0x314f + JungSungIdx[vcode - NUM_JASO_FC];
					}
					else
					{
						buf[1] = 0x3131 + JongSungIdx[vcode - ( NUM_JASO_FC + NUM_JASO_MV + 1 )];
					}
					buf[2] = 0;
				}
				else
				{
					buf[1] = '?';
				}
				#endif
			}
#if 0
			strcat( pt, "." );
#endif


			memset( &rq, 0, sizeof( rq ) );
#ifdef CKS	// HYMODIFY :: 2016.01.19
			rq.utf8_family_name = HY_UI_FAMILIES;
#else
			rq.utf8_family_name = SERIF_UI_FAMILIES;
#endif
			rq.point_size = 10;
			rq.weight = 500;
			font = GDrawInstanciateFont( NULL, &rq );
			GDrawSetFont( pixmap, font );
			GDrawWindowFontMetrics( pixmap, font, &as, &ds, &ld );

			txtWidth = 40;
			textX = bound.x + ( ( bound.width - txtWidth ) / 2 );
			textY = bound.y + bound.height - 5;

			GDrawDrawLine( pixmap, bound.x, y + bdf->pixelsize, bound.x + bound.width, y + bdf->pixelsize, bbcol );
			txtWidth = GDrawDrawText8( pixmap, textX, textY, utf8_buf, -1, 0x000000 );
		}
		else if( cv->linkview->viewmode == VIEW_UNICODE )
		{
			memset( &rq, 0, sizeof( rq ) );
#ifdef CKS	// HYMODIFY :: 2016.01.19
			rq.utf8_family_name = HY_UI_FAMILIES;
#else
			rq.utf8_family_name = SERIF_UI_FAMILIES;
#endif
			rq.point_size = 10;
			rq.weight = 300;
			font = GDrawInstanciateFont( NULL, &rq );
			GDrawSetFont( pixmap, font );
			GDrawWindowFontMetrics( pixmap, font, &as, &ds, &ld );

			txtWidth = 40;//20;
			textX = bound.x + ( ( bound.width - txtWidth ) / 2 );//x + 5;
			textY = bound.y + bound.height - 5;

			sprintf( utf8_buf, "%s", sc->name );
			GDrawDrawLine( pixmap, bound.x, y + bdf->pixelsize, bound.x + bound.width, y + bdf->pixelsize, bbcol );
			txtWidth = GDrawDrawText8( pixmap, textX, textY, utf8_buf, -1, 0x000000 );
		}
		/*********************************************************************************************/

		/*********************************************************************************************/
		// Draw Hangul Jaso VCode ( �� , �� , .... )
		buf[0] = 0x0020;
		if( sc->bCompositionUnit )
		{
			int vcode = sc->VCode;
			if( vcode < NUM_JASO_FC )
			{
				buf[1] = 0x3131 + ChoSungIdx[vcode];
			}
			else if( vcode < NUM_JASO_FC + NUM_JASO_MV )
			{
				buf[1] = 0x314f + JungSungIdx[vcode - NUM_JASO_FC];
			}
			else
			{
				buf[1] = 0x3131 + JongSungIdx[vcode - ( NUM_JASO_FC + NUM_JASO_MV + 1 )];
			}
			buf[2] = 0;
		}
		else
		{
			buf[1] = '?';
		}
		buf[1] = 0;

		textX = textX + txtWidth + 5;
		GDrawDrawText( pixmap, textX , textY, buf, -1, 0x000000 );
#if 0
		unichar_t jasobuf[2];
		int vcode = cv->b.sc->VCode;

		if( vcode < NUM_JASO_FC )
		{
			jasobuf[0] = 0x3131 + ChoSungIdx[vcode];
		}
		else if( vcode < NUM_JASO_FC + NUM_JASO_MV )
		{
			jasobuf[0] = 0x314f + JungSungIdx[vcode - NUM_JASO_FC];
		}
		else
		{
			jasobuf[0] = 0x3131 + JongSungIdx[vcode - ( NUM_JASO_FC + NUM_JASO_MV + 1 )];
		}
		jasobuf[1] = 0;
		hydebug( "<%x>\n", jasobuf );

		textX = textX + txtWidth + 5;
		GDrawDrawText( pixmap, textX , textY, jasobuf, -1, 0x000000 );
#endif

		GDrawDrawRect( pixmap, &bound, bbcol );
		GDrawPushClip( pixmap, &bound, &old );

		imgX = bound.x + bdfc->xmin;
		imgY = bound.y + bdf->ascent - bdfc->ymax;

		GDrawDrawImage( pixmap, &gi, NULL, imgX, imgY );
		GDrawPopClip( pixmap, &old );

		if ( !bdf->piecemeal )
		{
			BDFCharFree( bdfc );
		}
	}
	BDFFontFree( bdf );
	return;
}

static void HY_LinkViewExpose( GWindow pixmap, CharView *cv, GEvent *event )
{
	GRect	size;
	char		selName[128];
	char 	title[300];

	cv->linkview->depcount = HY_GetDependentCount( cv->b.sc );

	if( cv->linkview->depcount > 0 )
	{
		GDrawFillRect( pixmap, NULL, bgcol );
		GDrawGetSize( pixmap, &size );
		HY_LinkViewGetPageCount(cv);

		int		col					= 0;
		int		row					= 0;
		int		LastPageCharCnt		= 0;

		// Max Char Count
		if( cv->linkview->bb_w > 0 && cv->linkview->bb_h > 0 )
		{
			if( cv->linkview->width < cv->linkview->bb_w )
			{
				col = 1;
			}
			else
			{
				col = cv->linkview->width / cv->linkview->bb_w;
			}

			if( cv->linkview->height < cv->linkview->bb_h )
			{
				row = 1;
			}
			else
			{
				row = cv->linkview->height / cv->linkview->bb_h;
			}
			cv->linkview->maxcharcount = col * row;
			cv->linkview->maxcol = col;
			cv->linkview->maxrow = row;
		}
		else
		{
			hydebug( "=====================\n" );
			hydebug( " [ERROR] Invalid Window Size! \n" );
			hydebug( "=====================\n" );
			GDrawSetWindowTitles8( cv->linkview->gw, "Invalid Window Size", NULL );
			GDrawFillRect( pixmap, NULL, errbgcol );
			GDrawDrawText8( pixmap, 20, 20, _("Invalid Window Size"), -1, 0x000000 );
			return;
		}

		// Page Count
		cv->linkview->pagecount = cv->linkview->depcount / cv->linkview->maxcharcount;
		LastPageCharCnt = cv->linkview->depcount % cv->linkview->maxcharcount;
		if( LastPageCharCnt > 0 )
		{
			cv->linkview->pagecount += 1;
		}
		else
		{
			if( cv->linkview->pagecount == 0 )
			{
				hydebug( "=====================\n" );
				hydebug( " [ERROR] Invalid Char Count! \n" );
				hydebug( "=====================\n" );
				GDrawSetWindowTitles8( cv->linkview->gw, "Invalid Char Count", NULL );
				GDrawFillRect( pixmap, NULL, errbgcol );
				GDrawDrawText8( pixmap, 20, 20, _("Invalid Char Count"), -1, 0x000000 );
				return;
			}
			else
			{
				if( cv->linkview->maxcharcount <= cv->linkview->depcount )
				{
					cv->linkview->charcount = cv->linkview->maxcharcount;
				}
				else
				{
					cv->linkview->charcount = cv->linkview->depcount;
				}
				LastPageCharCnt = ( cv->linkview->pageindex * cv->linkview->maxcharcount ) + cv->linkview->charcount;
			}
		}

		if( cv->linkview->pagecount == 1 )
		{
			cv->linkview->charcount = LastPageCharCnt;
		}
		else
		{
			if( cv->linkview->pageindex < cv->linkview->pagecount - 1 )
			{
				cv->linkview->charcount = cv->linkview->maxcharcount;
			}
			else
			{
				cv->linkview->charcount = LastPageCharCnt;
			}
		}

		if( cv->linkview->charindex < 0 )// || cv->linkview->charindex > cv->linkview->depcount - 1 )
		{
			cv->linkview->charindex = 0;
			cv->linkview->selindex = 0;
		}

		if( cv->linkview->depcount )
		{
			if( cv->linkview->charindex > cv->linkview->depcount - 1 )
			{
				cv->linkview->charindex = cv->linkview->depcount - 1;
			}
		}

		if( cv->linkview->pageindex < 0 || cv->linkview->pageindex > cv->linkview->depcount )
		{
			printf( "=====================\n" );
			printf( " [ERROR] Invalid Page Index! \n" );
			printf( "=====================\n" );
			GDrawSetWindowTitles8( cv->linkview->gw, "Invalid Page Index", NULL );
			GDrawFillRect( pixmap, NULL, errbgcol );
			GDrawDrawText8( pixmap, 20, 20, _("Invalid Page Index"), -1, 0x000000 );
			return;
		}

		// Check Select Index
		if( cv->linkview->selindex > cv->linkview->charcount - 1 )
		{
			cv->linkview->selindex = cv->linkview->charcount - 1;
		}

		SplineChar * glyf = NULL;
		int gid = -1;
		int xoff = 0, yoff = 0;
		int DrawCharCount = 0, DrawPageCount = 0;;
		int i = 0, j = 0, k = 0;
		GRect r;

		for( i = 0 ; i < cv->b.fv->map->enccount ; i++ )
		{
			gid = cv->b.fv->map->map[i];
			if( gid != -1 )
			{
				glyf = cv->b.fv->sf->glyphs[gid];

				if( glyf != NULL && HY_IsDependents( glyf, cv->b.sc ) )
				{
					if( cv->linkview->pageindex < cv->linkview->pagecount )
					{
						if( j > cv->linkview->depcount )
						{
							break;
						}
						j++;

						DrawPageCount = k / cv->linkview->maxcharcount;
						k++;
						if( cv->linkview->pageindex == DrawPageCount )
						{
							r.x		= xoff + 1;
							r.y		= yoff + cv->linkview->bb_w + 1;
							r.width	= cv->linkview->bb_w - 1;
							r.height	= cv->linkview->name_h - 1;

#if 0
							if( HY_GetKSC5601( (uint16) glyf->unicodeenc ) )
							{
								GDrawFillRect( pixmap, &r, ksc5601col );
							}
#endif
#if 0
							if( i >= 0xAC00 && i <= 0xD7A3 )
							{
								if( cv->b.fv->sf->bBindo[i - 0xAC00] != 0 )
								{
									GDrawFillRect( pixmap, &r, bindocol );
								}
							}
#endif

							r.x		= xoff + 1;
							r.y		= yoff + 1;
							r.width	= cv->linkview->bb_w - 1;
							r.height	= cv->linkview->bb_w - 1;

							if( cv->linkview->selindex == DrawCharCount )
							{
								GDrawFillRect( pixmap, &r, selcol );
								cv->linkview->selectsc = glyf;
								sprintf(selName, "%s", glyf->name );
							}

							// Draw Glyph Image
							HY_DrawLinkViewGlyfs( pixmap, cv, glyf, i, gid, xoff, yoff );

							DrawCharCount++;
							xoff = xoff + cv->linkview->bb_w;
							if( ( xoff + cv->linkview->bb_w ) > cv->linkview->width )
							{
								xoff = 0;
								yoff = yoff + cv->linkview->bb_h;

								if( yoff >= cv->linkview->height )
								{
									break;
								}
							}
						}
					}
				}
			}
		}

		// Set CompoGlyfs View Title
		memset( title, 0x00, sizeof(title) );
		int encindex = HY_GetSCEncBySC( cv->b.sc->parent, cv->linkview->selectsc );
		sprintf( title, "(%d : %s | %s) | Size : %d (%f)", encindex, selName, cv->b.sc->name, cv->linkview->size, rint(( cv->linkview->size * 72.0 ) / cv->linkview->dpi ) );
		GDrawSetWindowTitles8( cv->linkview->gw, title, NULL );
		GDrawRequestExpose( cv->v, NULL, false );
	}
	else
	{
		GDrawFillRect( pixmap, NULL, errbgcol );
		sprintf( title, "None dependents chars..." );
		GDrawSetWindowTitles8( cv->linkview->gw, title, NULL );
		GDrawDrawText8( pixmap, 20, 20, _("None Dependents Glyphs"), -1, 0x000000 );
	}
}

static int linkview_e_h( GWindow gw, GEvent *event )
{
	CharView * cv = GDrawGetUserData( gw );

	if( ( event->type == et_mouseup || event->type == et_mousedown ) && (event->u.mouse.button == 4 || event->u.mouse.button == 5) )
	{
		return( GGadgetDispatchEvent(cv->linkview->vsb, event) );
	}

	switch( event->type )
	{
		case et_expose:
		{
			HY_LinkViewExpose( gw, cv, event );
		}
		break;
		case et_close:
		{
			if( cv->showlinkview != 0 )
			{
				cv->showlinkview = 0;
				if( cv->linkview->gw != NULL )
				{
					GDrawDestroyWindow(cv->linkview->gw);
				}
				free(cv->linkview);
				cv->linkview = NULL;
			}
		}
		break;
		case et_destroy:
		{
			if( cv->showlinkview != 0 )
			{
				cv->showlinkview = 0;
				if( cv->linkview->gw != NULL )
				{
					GDrawDestroyWindow(cv->linkview->gw);
				}
				free(cv->linkview);
				cv->linkview = NULL;
			}
		}
		break;
		case et_resize:
		{
			if ( event->u.resize.sized )
			{
				HY_LinkViewResize( cv );
				GDrawRequestExpose( gw, NULL, false );
			}
		}
		break;
		case et_char:
		{
			if( event->u.chr.state & ksm_control )
			{
				if( (event->u.chr.keysym == GK_Num_Dec) || (event->u.chr.keysym == GK_Dec) )
				{
					cv->linkview->size--;
					if( cv->linkview->size < 9 )
					{
						cv->linkview->size = 9;
					}
				}
				else if( (event->u.chr.keysym == GK_Num_Inc) || ((event->u.chr.keysym == GK_Inc) && (event->u.chr.state & ksm_shift)) )
				{
					cv->linkview->size++;
					if( cv->linkview->size > 512 )
					{
						cv->linkview->size = 512;
					}
				}
				cv->linkview->bb_w = cv->linkview->size;
				cv->linkview->bb_h = cv->linkview->size + cv->linkview->name_h;
				s_glyphsize = cv->linkview->size;
				HY_LinkViewCharResize(cv);
			}
			else
			{
				if( (event->u.chr.keysym == GK_Num_Dec) || (event->u.chr.keysym == GK_Dec) )
				{
					cv->linkview->size -= 10;
					if( cv->linkview->size < 9 )
					{
						cv->linkview->size = 9;
					}
				}
				else if( (event->u.chr.keysym == GK_Num_Inc) || ((event->u.chr.keysym == GK_Inc) && (event->u.chr.state & ksm_shift)) )
				{
					cv->linkview->size += 10;
					if( cv->linkview->size > 512 )
					{
						cv->linkview->size = 512;
					}
				}
				cv->linkview->bb_w = cv->linkview->size;
				cv->linkview->bb_h = cv->linkview->size + cv->linkview->name_h;
				s_glyphsize = cv->linkview->size;
				HY_LinkViewCharResize(cv);
			}

			if( event->u.chr.keysym == GK_Left )
			{
				HY_LinkViewSelectGlyph(cv, -1);
			}

			if( event->u.chr.keysym == GK_Right )
			{
				HY_LinkViewSelectGlyph(cv, 1);
			}

			if( event->u.chr.keysym == GK_Up )
			{
				HY_LinkViewSelectGlyph(cv, -cv->linkview->maxcol );
			}

			if( event->u.chr.keysym == GK_Down )
			{
				HY_LinkViewSelectGlyph(cv, cv->linkview->maxcol );
			}

			if( event->u.chr.keysym == GK_Page_Up )
			{
				HY_LinkViewMovePageIndex(cv, 0);
			}

			if( event->u.chr.keysym == GK_Page_Down )
			{
				HY_LinkViewMovePageIndex(cv, 1);
			}

			if( event->u.chr.keysym == GK_F1 )
			{
				cv->linkview->viewmode = !(cv->linkview->viewmode);
			}

			if( event->u.chr.keysym == GK_Return )
			{
				CharViewCreate( cv->linkview->selectsc, (FontView *)cv->b.fv, cv->linkview->selectsc->unicodeenc );
			}
			GDrawRequestExpose( gw, NULL, false );

#if LINKVIEW_DEBUG
			hydebug( "==============================================\n" );
			hydebug( "[%s] Select SC: <%s>\n", __FUNCTION__, cv->linkview->selectsc->name );
			hydebug( "[%s] Select Index: (%d)\n", __FUNCTION__, cv->linkview->selindex );
			hydebug( "[%s] Page index:(%d) / count:(%d)\n", __FUNCTION__, cv->linkview->pageindex, cv->linkview->pagecount );
			hydebug( "[%s] Char index:(%d) / count:(%d)\n", __FUNCTION__, cv->linkview->charindex, cv->linkview->charcount );
			hydebug( "==============================================\n" );
#endif
		}
		break;
		case et_mousedown:
		{
			int xpos = event->u.mouse.x / cv->linkview->bb_w;
			int ypos = event->u.mouse.y / cv->linkview->bb_h;
			int index = 0;

			if( ypos > 0 )
			{
				index = xpos + (cv->linkview->maxcol * ypos);
			}
			else
			{
				index = xpos;
			}

			if( index < cv->linkview->charcount )
			{
				cv->linkview->selindex = index;
			}
			GDrawRequestExpose( gw, NULL, false );

#if LINKVIEW_DEBUG
			hydebug( "==============================================\n" );
			hydebug( "[%s] Select SC: <%s>\n", __FUNCTION__, cv->linkview->selectsc->name );
			hydebug( "[%s] Select Index: (%d)\n", __FUNCTION__, cv->linkview->selindex );
			hydebug( "[%s] Page index:(%d) / count:(%d)\n", __FUNCTION__, cv->linkview->pageindex, cv->linkview->pagecount );
			hydebug( "[%s] Char index:(%d) / count:(%d)\n", __FUNCTION__, cv->linkview->charindex, cv->linkview->charcount );
			hydebug( "==============================================\n" );
#endif
		}
		break;
		case et_mouseup:
		{
			if ( event->type == et_mouseup && event->u.mouse.clicks == 2 )
			{
				CharViewCreate( cv->linkview->selectsc, (FontView *)cv->b.fv, cv->linkview->selectsc->unicodeenc );
			}
		}
		break;
		case et_controlevent:
		{
			switch( event->u.control.subtype )
			{
				case et_scrollbarchange:
				{
					HY_LinkViewVScroll(cv, &event->u.control.u.sb);
					GDrawRequestExpose( gw, NULL, false );
				}
				break;
			}
		}
		break;
	}
	return( true );
}

void HY_ResetLinkView( CharView * cv, int flags )
{
	int sbsize;
	int oldpageindex = 0;

	oldpageindex = cv->linkview->pageindex;
	HY_LinkViewGetPageCount(cv);

	sbsize = GDrawPointsToPixels(cv->linkview->gw, SCROLL_BAR_WIDTH);

	if( flags )
	{
		if( oldpageindex > cv->linkview->pagecount -1 )
		{
			cv->linkview->pageindex = cv->linkview->pagecount -1;
		}

		if( cv->linkview->charindex > cv->linkview->depcount -1 )
		{
			cv->linkview->charindex = cv->linkview->depcount -1;
			cv->linkview->selindex = cv->linkview->lastpagecharcount - 1;
			cv->linkview->scroll_off = cv->linkview->pagecount - 1;
		}

		if( cv->linkview->charindex  > cv->linkview->maxcharcount )
		{
			cv->linkview->pageindex = cv->linkview->charindex  / cv->linkview->maxcharcount;
			cv->linkview->selindex = cv->linkview->charindex % cv->linkview->maxcharcount;
		}
	}
	else
	{
		cv->linkview->pageindex = 0;
		cv->linkview->charindex = 0;
		cv->linkview->selindex = 0;
		cv->linkview->scroll_off = 0;
	}

	if( cv->linkview->pageindex == cv->linkview->pagecount -1 )
	{
		cv->linkview->width = (cv->linkview->bb_w * cv->linkview->maxcol) + sbsize;
		cv->linkview->height = DEFAULT_MAX_ROW_NUM * cv->linkview->bb_h;
	}
	else
	{
		cv->linkview->width = (cv->linkview->bb_w * cv->linkview->maxcol) + sbsize;
		cv->linkview->height = cv->linkview->maxrow * cv->linkview->bb_h;
	}

	GDrawResize( cv->linkview->gw, cv->linkview->width, cv->linkview->height );

	GGadgetMove(cv->linkview->vsb, cv->linkview->width - sbsize, 0);
	GGadgetResize(cv->linkview->vsb, sbsize, cv->linkview->height);
	GScrollBarSetBounds(cv->linkview->vsb, 0, cv->linkview->pagecount, 1 );
	GScrollBarSetPos(cv->linkview->vsb, cv->linkview->scroll_off);
	HY_LinkViewSetPageIndex(cv, cv->linkview->scroll_off);
}

void HY_DoLinkView( CharView * cv )
{
	GWindowAttrs wattrs;
	GRect size;
	GRect pos;
	int sbsize;
	GGadgetData gd;
	LinkView * linkview;

	memset( &wattrs, 0, sizeof( wattrs ) );
	wattrs.mask			= wam_events | wam_cursor | wam_utf8_wtitle | wam_utf8_ititle | wam_noresize;
	wattrs.event_masks	= -1;
	wattrs.cursor			= ct_mypointer;
	wattrs.utf8_window_title	= "Show Link Info";

	GDrawGetSize( cv->gw, &size );

#if 1
	pos.x = size.x + size.width;
	pos.y = size.y + (size.y * 0.3);
	pos.width = (size.width * 0.5);
	pos.height = size.height;
#else
	pos.x = size.x + size.width + SCROLL_BAR_WIDTH_PX;
	pos.y = size.y;
	pos.width = size.width;
	pos.height = size.height;
#endif

	cv->linkview = calloc( 1, sizeof( LinkView ) );
	cv->linkview->gw = GDrawCreateTopWindow( NULL, &pos, linkview_e_h, cv, &wattrs );
	sbsize = GDrawPointsToPixels(cv->linkview->gw, SCROLL_BAR_WIDTH);

	cv->showlinkview					= 1;
	cv->linkview->charindex			= 0;
	cv->linkview->selindex			= 0;
	cv->linkview->pageindex			= 0;
	cv->linkview->rowcnt				= 0;
	cv->linkview->charcount			= 0;
	cv->linkview->maxcharcount		= 0;
	cv->linkview->lastlinecharcount		= 0;
	cv->linkview->lastpagecharcount	= 0;
	cv->linkview->pagecount			= 0;
	cv->linkview->scroll_off			= 0;
	cv->linkview->viewmode			= VIEW_HANGUL;
	cv->linkview->movemode			= false;
	cv->linkview->dpi					= DEFAULT_GLYFS_DPI;
	cv->linkview->size				= DEFAULT_GLYFS_SIZE;
	cv->linkview->name_w			= cv->linkview->size;
	cv->linkview->name_h				= DEFAULT_NAME_BOX_HEIGHT;
	cv->linkview->bb_w				= cv->linkview->size;
	cv->linkview->bb_h				= cv->linkview->size + cv->linkview->name_h;
	cv->linkview->maxcol				= DEFAULT_MAX_COL_NUM;
	cv->linkview->maxrow				= DEFAULT_MAX_ROW_NUM;
	cv->linkview->width				= cv->linkview->maxcol * cv->linkview->bb_w + sbsize;
	cv->linkview->height				= cv->linkview->maxrow * cv->linkview->bb_h;
	cv->linkview->selectsc				= NULL;

	memset( &gd, 0, sizeof(gd) );
	gd.pos.x		= pos.width - sbsize;
	gd.pos.y		= 0;
	gd.pos.width	= sbsize;
	gd.pos.height	= cv->linkview->height;
	gd.flags		= gg_visible | gg_enabled | gg_pos_in_pixels | gg_sb_vert;

	cv->linkview->vsb = GScrollBarCreate(cv->linkview->gw, &gd, cv->linkview);
	GScrollBarSetBounds(cv->linkview->vsb, 0, cv->linkview->pagecount, 1 );
	GScrollBarSetPos(cv->linkview->vsb, 0);

	HY_LinkViewGetPageCount(cv);
	cv->linkview->width = (cv->linkview->bb_w * cv->linkview->maxcol) + sbsize;
	cv->linkview->height = (cv->linkview->bb_h * cv->linkview->maxrow);
	HY_LinkViewCharResize(cv);

	int i = 0, index = 0, gid;
	SplineChar * sc;
	int depcnt = HY_GetDependentCount(cv->b.sc );

	if( depcnt > 0 )
	{
		for( i = 0 ; i < cv->b.fv->map->enccount ; i++ )
		{
			gid = cv->b.fv->map->map[i];
			sc = cv->b.fv->sf->glyphs[gid];
			if( gid != -1 && sc != NULL && HY_IsDependents( sc, cv->b.sc ) )
			{
				if( cv->linkview->parentsc != NULL )
				{
					if( strcmp( cv->linkview->parentsc->name, sc->name ) == 0 )
					{
						cv->linkview->charindex = index;
						cv->linkview->selectsc = sc;
					}
				}
				index++;
			}
		}
		cv->linkview->pageindex = cv->linkview->charindex / cv->linkview->maxcharcount;
		cv->linkview->selindex = cv->linkview->charindex % cv->linkview->maxcharcount;
		cv->linkview->scroll_off = cv->linkview->pageindex;

		GGadgetMove(cv->linkview->vsb, cv->linkview->width - sbsize, 0);
		GGadgetResize(cv->linkview->vsb, sbsize, cv->linkview->height);
		GScrollBarSetBounds(cv->linkview->vsb, 0, cv->linkview->pagecount, 1 );
		GScrollBarSetPos(cv->linkview->vsb, cv->linkview->scroll_off);
		HY_LinkViewSetPageIndex(cv, cv->linkview->scroll_off);

	}
	GDrawSetVisible( cv->linkview->gw, true );
	GDrawRequestExpose( cv->linkview->gw, NULL, false );
}

// END OF FILE

